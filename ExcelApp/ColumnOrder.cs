﻿namespace ExcelApp
{
    class ColumnOrder
    {
        public string Name { get; set; }
        public string Barcode { get; set; }
        public double Quantity { get; set; }
        public string Size { get; set; }
        public double Price { get; set; }
        public string Batch { get; set; }
        public string Cartoon { get; set; }
        public string Q { get; set; }
    }
}
