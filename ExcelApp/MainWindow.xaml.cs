﻿using System;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text.RegularExpressions;

namespace ExcelApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Excel.Application _excelApp;
        private Excel.Window _excelWindow;
        private Excel.Workbook _workbook;
        private Excel.Workbooks _workbooks;
        private Excel.Sheets _sheets;
        private Excel.Sheets _sheetsPrice;
        private Excel.Worksheet _worksheet;
        private Excel.Worksheet _sheetToWrite;
        private Excel.Range _cells;
        private Excel.Range _cellsToWrite;
        private Excel.Worksheet _worksheetPrice;


        private enum OutputColumns
        {
            Name = 1,
            Barcode = 2,
            Quantity = 3,
            Size = 4,
            Price = 5,
            Batch = 6,
            Cartoon = 7,
            Q = 8
        }

        private enum StaticDataOrder
        {
            Cartoon = 1,
            Article = 3,
            Quarter = 4,
        }

        public MainWindow()
        {
            InitializeComponent();

            _excelApp = new Excel.Application { Visible = true };
        }


        private const ushort BottomOffset = 0;
        private const ushort TopOffset = 10;
        private const ushort LeftOffset = 5;
        private const ushort RighOffset = 1;
        private string PreviousCartoon = null;
        private const int sizeColumnOffset = 1;  // Отступ от ячейки артикла в книге ean codes в которой находится размер
        private const int barcodeColumnOffset = 2; // Отступ от ячейки артикла в книге ean codes в которой находится штрихкод
        private const int qOffset = 2;

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            _excelApp.Workbooks.Open(packListDialog.Text,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing);
            _excelApp.Workbooks.Open(ean.Text,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing);
            _excelApp.Workbooks.Open(Directory.GetCurrentDirectory() + "/result.xlsx",
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing);
            _excelApp.Workbooks.Open(unitedDialog.Text,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                     Type.Missing, Type.Missing, Type.Missing);

            _workbooks = _excelApp.Workbooks;
            _workbook = _workbooks[1];
            _sheets = _workbook.Worksheets;

            //Присваеваем рабочему листу Packing list откуда считываем кол-во размер
            _worksheet = (Excel.Worksheet)_sheets.Item[1];

            Excel.Range lastOccupiedCell = _worksheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell,
                                                                         Type.Missing);
            _cells = _worksheet.Range["A1", lastOccupiedCell];


            // Присваивание для вывода результатов
            _workbook = _workbooks[2];
            Excel.Sheets searchSheets = _workbook.Worksheets;
            Excel.Worksheet searchSheet = (Excel.Worksheet)searchSheets.Item[1];
            lastOccupiedCell = searchSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell,
                                                              Type.Missing);
            Excel.Range searchRange = searchSheet.Range["A1", lastOccupiedCell];

            // Присваивание для поиска штрихкодов
            _workbook = _workbooks[3];
            _sheets = _workbook.Worksheets;
            _sheetToWrite = (Excel.Worksheet)_sheets.Item[1];

            _workbook = _workbooks[4];
            _sheetsPrice = _workbook.Worksheets;
            _worksheetPrice = (Excel.Worksheet)_sheets.Item[1];

            int rowsCount = _cells.Rows.Count;
            int columnsCount = _cells.Columns.Count;

            // Цикл по каждой строке в таблице Packing list
            for (int i = TopOffset; i <= rowsCount - BottomOffset; i++)
            {

                if (_cells[i, 1].Value2 != null)
                {
                    PreviousCartoon = _cells[i, 1].Value2.ToString();
                }

                //Console.WriteLine(i);
                // Цикл по каждому столбцу в таблице Packing list
                for (int j = LeftOffset; j <= columnsCount - RighOffset; j++)
                {

                    if (_cells[i, j].Value2 == null)
                    {
                        continue;
                    }

                    ColumnOrder co = new ColumnOrder();
                    // Партия
                    co.Batch = Q.Text;
                    // Сезон
                    co.Q = _cells[i, StaticDataOrder.Quarter].Value2;
                    // Наименование
                    co.Name = _cells[i, StaticDataOrder.Article].Value2;
                    // Количество

                    try
                    {
                        co.Quantity = _cells[i, j].Value2;
                    }
                    catch (Exception ex)
                    {
                        co.Quantity = 0;
                    }

                    // Коробка

                    co.Cartoon = PreviousCartoon;

                    // Цикл для поиска Штрихкода в трех листах ean
                    for (int k = 1; k <= 3; k++)
                    {
                        bool flag = false;

                        searchSheet = (Excel.Worksheet)searchSheets.Item[k];
                        lastOccupiedCell = searchSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell,
                                                                              Type.Missing);
                        searchRange = searchSheet.Range["A1", lastOccupiedCell];

                        Excel.Range finded = searchRange.Find(_cells[i, StaticDataOrder.Article].Value2);

                        if (finded != null)
                        {
                            int firstFindedRow = finded.Row;

                            // Поиск в первом листе
                            if (k == 1)
                            {
                                //Если размер из Packing list == размеру из ean codes
                                if (finded.Offset[0, sizeColumnOffset].Value2 == _cells[4, j].Value2)
                                {
                                    flag = true;
                                    // Размер
                                    co.Size = finded.Offset[0, sizeColumnOffset].Value2;
                                    // Штрихкод
                                    co.Barcode = ValidateBarcode(finded.Offset[0, barcodeColumnOffset].Value2);
                                }
                                else
                                {
                                    // Ищем дальше
                                    // Цикл для нахождения подходящего размера
                                    while (finded != null)
                                    {
                                        finded = searchRange.FindNext(finded);

                                        if (finded.Row == firstFindedRow)
                                        {
                                            break;
                                        }

                                        //Если размер из Packing list == размеру из ean codes
                                        if (finded.Offset[0, sizeColumnOffset].Value2 == _cells[4, j].Value2)
                                        {
                                            // Размер
                                            co.Size = finded.Offset[0, sizeColumnOffset].Value2;
                                            // Штрихкод
                                            co.Barcode = ValidateBarcode(finded.Offset[0, barcodeColumnOffset].Value2);

                                            finded = null;
                                            flag = true;
                                        }
                                    }
                                }

                                // Поиск во втором листе
                            }
                            else if (k == 2)
                            {
                                //Если размер из Packing list == размеру из ean codes
                                if (finded.Offset[0, sizeColumnOffset].Value2.ToString().Replace(",", ".") == _cells[5, j].Value2.ToString().Replace(",", "."))
                                {
                                    flag = true;
                                    // Размер
                                    co.Size = finded.Offset[0, sizeColumnOffset].Value2.ToString().Replace(",", ".");
                                    // Штрихкод
                                    co.Barcode = ValidateBarcode(finded.Offset[0, barcodeColumnOffset].Value2);
                                }
                                else
                                {
                                    // Ищем дальше
                                    // Цикл для нахождения подходящего размера

                                    while (finded != null)
                                    {
                                        finded = searchRange.FindNext(finded);

                                        if (finded.Row == firstFindedRow)
                                        {
                                            break;
                                        }

                                        if (finded.Offset[0, sizeColumnOffset].Value2.ToString().Replace(",", ".") == _cells[5, j].Value2.ToString().Replace(",", "."))
                                        {
                                            // Размер
                                            co.Size = finded.Offset[0, sizeColumnOffset].Value2.ToString();
                                            // Штрихкод
                                            co.Barcode = ValidateBarcode(finded.Offset[0, barcodeColumnOffset].Value2);

                                            finded = null;
                                            flag = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // Размер
                                co.Size = finded.Offset[0, sizeColumnOffset].Value2.ToString();
                                // Штрихкод
                                co.Barcode = ValidateBarcode(finded.Offset[0, barcodeColumnOffset].Value2);

                                Console.WriteLine("Goods with article {0} and with size {1} or {2} doesn't finded", _cells[i, StaticDataOrder.Article].Value2, _cells[4, j].Value2, _cells[5, j].Value2);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Goods with article {0} doesn't finded", _cells[i, StaticDataOrder.Article].Value2);
                        }
                        if (flag)
                        {
                            break;
                        }
                    }

                    // Поиск цены

                    for (int p = 2; p <= 4; p++)
                    {
                        _worksheetPrice = (Excel.Worksheet)_sheetsPrice.Item[p];
                        lastOccupiedCell = _worksheetPrice.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell,
                                                                              Type.Missing);
                        Excel.Range sRange = _worksheetPrice.Range["A1", lastOccupiedCell];

                        Console.WriteLine(_cells[i, 2].Value2);

                        string article = _cells[i, 2].Value2;
                        string letter = article.Substring(article.Length - 1, 1);
                        if (!Regex.IsMatch(letter, @"^\d+$"))
                        {
                            article = article.Substring(0, article.Length - 1);
                        }

                        Excel.Range finded = sRange.Find(article);
                        if (finded != null)
                        {
                            co.Price = finded.Offset[0, 1].Value2;
                            break;
                        }
                    }

                    Write(co);
                }
            }

            _excelApp.Workbooks[4].Close();
            _excelApp.Workbooks[2].Close();
            _excelApp.Workbooks[1].Close();

            MessageBox.Show("Все рассчеты завершены");
        }

        private string ValidateBarcode(object value)
        {
            string tmp = "";
            if (value is double)
            {
                tmp = value.ToString();
                if (tmp.Length > 0)
                {
                    if (tmp.IndexOf(".") > 0)
                    {
                        return tmp.Substring(0, tmp.IndexOf("."));
                    }
                    if (tmp.IndexOf(",") > 0)
                    {
                        return tmp.Substring(0, tmp.IndexOf(","));
                    }
                }
            }
            return value.ToString();
        }


        private static int RowToWrite = 1;
        private void Write(ColumnOrder co)
        {
            Console.WriteLine(String.Format("Barcode is {0}, Name is {1}, Size is {2}, Quantity is {3}, Price is {4}", co.Barcode, co.Name, co.Size, co.Quantity, co.Price));

            _sheetToWrite.Cells[RowToWrite, OutputColumns.Name] = co.Name;
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Size].NumberFormat = "@";
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Size] = co.Size;
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Quantity] = co.Quantity;
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Barcode].NumberFormat = "@";
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Barcode] = co.Barcode;
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Price] = co.Price;
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Batch] = co.Batch;
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Cartoon] = co.Cartoon;
            _sheetToWrite.Cells[RowToWrite, OutputColumns.Q] = co.Q;
            RowToWrite++;
        }


        private void button3_Click(object sender, RoutedEventArgs e)
        {
            _excelApp.Workbooks.Close();
            _excelApp.Quit();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".xls";
            dlg.Filter = "Excel 2007 Files (*.xls)|*.xls|Excel 2010 Files (*.xlsx)|*.xlsx";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                packListDialog.Text = filename;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".xls";
            dlg.Filter = "Excel 2007 Files (*.xls)|*.xls|Excel 2010 Files (*.xlsx)|*.xlsx";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                unitedDialog.Text = filename;
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".xls";
            dlg.Filter = "Excel 2007 Files (*.xls)|*.xls|Excel 2010 Files (*.xlsx)|*.xlsx";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                ean.Text = filename;
            }
        }
    }
}
